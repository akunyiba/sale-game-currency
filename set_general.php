


<div style="margin-left:50px; margin-top:20px; ">

<!-- Блок общих настроек -->


<!-- Блок настроек игры -->
    <h3>Общие настройки</h3>
<form class="form-horizontal" action="admin.php?page=edit-settings&action=submit" method="POST"  name="gamefield" id="sale-gold">
        <div class="form-group ">
        <input  type="hidden" name="field" value="game" />
        <label  class="col-xs-2 control-label">Выберите игру</label>
        <div class="col-xs-3">
        <select  id="inputGame" class="form-control" name="name_field">
        
        <?php if (count($this->data['games']) > 0): ?>
        <?php foreach ($this->data['games'] as $key => $record): ?>
            <option value="<?php echo $record['ID'] ?>">
            <?php echo $record['name_game'] ?></option> 
        <?php endforeach; ?>
        <?php else:?>
        <option value="no">Игр нет</option>
        <?php endif;?>
        </select>
        </div>
        <div class="col-xs-2">
        
        <button type="submit" name="submit" value="edit" class="btn btn-primary btn-sm btn-block">Редактировать</button>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="add" class="btn btn-primary btn-sm btn-block">Добавить</button>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="del" onclick="return deleteService(); " class="btn btn-primary btn-sm btn-block">Удалить</button>
        </div>
 </div>
</form> 
    <!-- Конец блока настроек игры -->
    <!-- Блок настроек сервера -->
    
<form class="form-horizontal" action="admin.php?page=edit-settings&action=submit" method="POST"  name="zakaz" id="sale-gold">
        <div class="form-group ">
        <input type="hidden" name="field" value="server" />
        <label  class="col-xs-2 control-label">Выберите сервер</label>
        <div class="col-xs-3">
        <select  id="inputServer" class="form-control" name="name_field">
        <?php if (count($this->data['servers']) > 0): ?>
        <?php foreach ($this->data['servers'] as $key => $record): ?>
            <option value="<?php echo $record['ID'] ?>">
            <?php echo $record['name_server'] ?></option>
            
        <?php endforeach; ?>
        <?php else:?>
        <option value="no">Серверов нет</option>
        <?php endif;?>
        </select>
        </div>
        <div class="col-xs-2">
        
        <button type="submit" name="submit" value="edit" class="btn btn-primary btn-sm btn-block">Редактировать</button>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="add" class="btn btn-primary btn-sm btn-block">Добавить</button>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="del" onclick="return deleteService(); " class="btn btn-primary btn-sm btn-block">Удалить</button>
        </div>
 </div>
</form> 
    <!-- Конец блока настроек сервера -->

     <!-- Блок настроек способа доставки -->
<form class="form-horizontal " action="admin.php?page=edit-settings&action=submit" method="post"  name="zakaz" id="sale-gold">
<input type="hidden" name="field" value="delivery" />
        <div class="form-group ">
        <label  class="col-xs-2 control-label">Способ доставки</label>
        <div class="col-xs-3">
        <select  id="inputServer" class="form-control" name="name_field">
            <?php if (count($this->data['deliveries']) > 0): ?>
        <?php foreach ($this->data['deliveries'] as $key => $record): ?>
            <option value="<?php echo $record['ID'] ?>">
            <?php echo $record['delivery'] ?></option>
            
        <?php endforeach; ?>
        <?php else:?>
        <option value="no">Способа нет</option>
        <?php endif;?>
        </select>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="edit" class="btn btn-primary btn-sm btn-block">Редактировать</button>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="add" class="btn btn-primary btn-sm btn-block">Добавить</button>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="del" onclick="return deleteService(); "  class="btn btn-primary btn-sm btn-block">Удалить</button>
        </div>
 </div>
</form> 
    <!-- Конец блока настроек способа доставки -->
     <!-- Блок настроек системы оплаты -->
<form class="form-horizontal " action="admin.php?page=edit-settings&action=submit" method="post"  name="zakaz" id="sale-gold">
<input type="hidden" name="field" value="payment" />
        <div class="form-group ">
        <label  class="col-xs-2 control-label">Система оплаты</label>
        <div class="col-xs-3">
        <select  id="inputServer" class="form-control" name="name_field">
        <?php if (count($this->data['payments']) > 0): ?>
        <?php foreach ($this->data['payments'] as $key => $record): ?>
            <option value="<?php echo $record['ID'] ?>">
            <?php echo $record['title_payment'] ?></option>
            
        <?php endforeach; ?>
        <?php else:?>
        <option value="no">Системы нет</option>
        <?php endif;?>
        </select>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="edit" class="btn btn-primary btn-sm btn-block">Редактировать</button>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="add" class="btn btn-primary btn-sm btn-block">Добавить</button>
        </div>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="del" onclick="return deleteService(); "  class="btn btn-primary btn-sm btn-block">Удалить</button>
        </div>
 </div>
</form> 
    <!-- Конец блока настроек системы оплаты -->   
    <!-- Блок настроек скидки -->
    <div>
<form class="form-horizontal" action="admin.php?page=edit-settings&action=submit" method="post"  name="zakaz2" id="sale-gold">
    <h3><strong>Настройка скидок и адреса страницы обработки платежа</strong></h3>
    
    <input type="hidden" name="field" value="skidki" />
    <div class="checkbox">
    <label>
      <input type="checkbox" name="status" value="1"<?php if ($this->data['skidki']['status']== 1) echo "checked"; ;?> > Использовать скидки
    </label>
  </div>
    <div class="form-group">						
       <div class="controls controls-row">
  		<div class="col-xs-2">
            <div class="add-up">
                <small id='paymethod_currency'>Сумма, больше чем</small>
            </div>
            <input  class="form-control" id="sum1" name="sum1" type="text" value="<?php echo $this->data['skidki']['sum1'];?>" />
        </div>
        <div class="col-xs-2">
            <div class="add-up">
                <small id='paymethod_currency'>Проценты</small>
            </div>
            <input id="percent1" name="percent1" class="form-control" value="<?php echo $this->data['skidki']['percent1'];?>" type="text" />
        </div>
        </div>
    </div>
   <div class="form-group">						
       <div class="controls controls-row">
  		<div class="col-xs-2">
            <div class="add-up">
                <small id='paymethod_currency'>Сумма, больше чем</small>
            </div>
            <input  class="form-control" id="sum2" name="sum2" type="text" onclick="runajax();" value="<?php echo $this->data['skidki']['sum2'];?>" />
        </div>
        <div class="col-xs-2">
            <div class="add-up">
                <small id='paymethod_currency'>Проценты</small>
            </div>
            <input id="percent2" name="percent2" class="form-control" value="<?php echo $this->data['skidki']['percent2'];?>" type="text" />
        </div>
        </div>
    </div>
    <div class="form-group">						
       <div class="controls controls-row">
  		<div class="col-xs-2">
            <div class="add-up">
                <small id='paymethod_currency'>Сумма, больше чем</small>
            </div>
            <input  class="form-control" id="sum3" name="sum3" type="text" value="<?php echo $this->data['skidki']['sum3'];?>" />
        </div>
        <div class="col-xs-2">
            <div class="add-up">
                <small id='paymethod_currency'>Проценты</small>
            </div>
            <input id="percent3" name="percent3" class="form-control" value="<?php echo $this->data['skidki']['percent3'];?>" type="text" />
        </div>
        </div>
    </div>
    <div class="form-group">						
       <div class="controls controls-row">
  		<div class="col-xs-2">
            <div class="add-up">
                <small id='paymethod_currency'>Сумма, больше чем</small>
            </div>
            <input  class="form-control" id="sum4" name="sum4" type="text" value="<?php echo $this->data['skidki']['sum4'];?>" />
        </div>
        <div class="col-xs-2">
            <div class="add-up">
                <small id='paymethod_currency'>Проценты</small>
            </div>
            <input id="percent4" name="percent4" class="form-control" value="<?php echo $this->data['skidki']['percent4'];?>" type="text" />
        </div>
        </div>
    </div>
    <h4>Result URL(для системы оплаты)</h4>
    <div class="form-group">
        
        <div class="col-xs-4">
        <input id="inputLink" class="form-control" name="link" placeholder="" type="text" value="<?php echo $this->data['skidki']['link'];?>" />
        <div class=" add-down">
		<small id="about">Создайте пустую страницу для обработки платежа и поместите ссылку сюда например: http://ваш домен/<strong>result</strong>/  </small>
		</div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-1 control-label" ></label>
        <div class="col-xs-2">
        <button type="submit" name="submit" value="save" class='btn btn-success btn-block'>Сохранить</button>
        </div>
    </div>
</form>
</div>
</div>
<!-- Конец блока скидки -->
    
<!-- Конец блока общих настроек -->