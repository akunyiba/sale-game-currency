<script>
jQuery(document).ready( function() {
    
jQuery.fn.reset = function () {
  jQuery(this).each (function() { this.reset(); });
}

jQuery('#sale-gold').reset();

var counter = 0;
 
 
jQuery("#kupon").change( function() {
            kupon=jQuery(this).val();
            email=jQuery('#inputEmail').val();
if(counter<1){          
  jQuery.ajax({
         type : "post",
         dataType : "json",
         url : MyAjax.ajaxurl,
         data : {action: "myajax-submit", kupon : kupon, email : email },
         async:false,
         success: function(skidka) 
         {
          
             if(skidka == "0")
             {
               jQuery("#res_kupon").text('Этот купон не действителен, возможно вы ошиблись!');
             }
             else 
             {  
             kup_skidka = skidka;
                d=jQuery("#dengi").val();
                deng = count_kupon (d);
                jQuery("#dengi").val(deng);
                jQuery("#res_kupon").text('Текущая скидка ' + kup_skidka+'%');  
                ++counter;
             }
         }
      });
     }     
});

function count_kupon (dengi_k){
  
  proc = dengi_k/100;
  deng = (dengi_k - (kup_skidka*proc)).toFixed(2);
  
  
  jQuery("#kup_skid").val((kup_skidka*proc).toFixed(2));
  jQuery("#kup_skid_pr").val(kup_skidka);
 // alert (deng);
  return deng;
}


    
var ser = <?php echo $servers; ?>;
var pay = <?php echo $payments; ?>;
var skid = <?php echo $skidki; ?>;

if (skid){
  for(it in skid)
  {
    s=skid[it];
    sum1= s.sum1;
    per1= s.percent1;
    sum2= s.sum2;
    per2= s.percent2;
    sum3= s.sum3;
    per3= s.percent3;
    sum4= s.sum4;
    per4= s.percent4;
    
  }
}

    jQuery("#inputServer ").click( function() {
            serverID=jQuery(this).val() || jQuery(this).text();
            if (serverID == '0') jQuery("#rate_server").text('');
                    
            for(item in ser){
                s=ser[item];
                if(s['ID'] ==serverID){
                    min= s.min_sum;
                    name=s.name_server;
                    rate=s.server_price;
                    jQuery("#rate_server").text('1 монета '+s.server_price+' руб. Минимум : '+min);
                    jQuery('#n_server').val(name);
                   }
            }
    key_valuta();
    }); 
    
    jQuery("#inputPayment").click( function() {
           payID=jQuery(this).val() || jQuery(this).text();
           for(item in pay){
              p=pay[item];
                if(p['ID'] == payID){
                    curs= p.rate_to_rub;
                    name=p.name_valuta;
                    title_payment=p.title_payment;
                    //comiss=pay.comiss_service;
                    jQuery("#n_valuta").text(name);
                    jQuery("#name_valuta").val(name);
                    jQuery('#n_pay').val(title_payment);
                   }
            }
    key_valuta();
    }); 
 jQuery("#dengi").bind('keyup change', function() {
    if(jQuery('#inputServer').val() == 0){ jQuery('#dengi').val( '' );jQuery("#rate_server").text(''); }
    //alert('dengi'); 
    key_dengi();
});
jQuery("#valuta ").bind('keyup change', function() {
    if(jQuery('#inputServer').val() == 0) {jQuery('#valuta').val( '' ); jQuery("#rate_server").text('');}
    key_valuta();
});
function add_skidka_val (money,valuta){
    
    valuta=parseInt(valuta);
    money=parseFloat(money);
    sum1=parseInt(sum1);
    sum2=parseInt(sum2);
    sum3=parseInt(sum3);
    sum4=parseInt(sum4); 
    
    s_money=Math.round(money/rate);
    if(s_money<=sum1) {s_money = Math.round(((money*100)/(100-per1))/rate);
    }
    if((s_money >= sum1) && (s_money < sum2)){
        valuta = Math.round(((money*100)/(100-per1))/rate);
        proc = (valuta*rate)/100;
        jQuery("#cur_skid").val((per1*proc).toFixed(2));
        jQuery("#cur_skid_pr").val(per1);
      
    }
    
    if(s_money<=sum2) {s_money = Math.round(((money*100)/(100-per2))/rate);
    }
    
    if((s_money >= sum2) && (s_money < sum3)){ 
        valuta = Math.round(((money*100)/(100-per2))/rate);
        proc = (valuta*rate)/100;
        jQuery("#cur_skid").val((per2*proc).toFixed(2));
        jQuery("#cur_skid_pr").val(per2);
    }
   
    if(s_money<=sum3) { s_money = Math.round(((money*100)/(100-per3))/rate);
    }
     
    if((s_money >= sum3) && (s_money < sum4)){
        valuta = Math.round(((money*100)/(100-per3))/rate);
        proc = (valuta*rate)/100;
        jQuery("#cur_skid").val((per3*proc).toFixed(2));
        jQuery("#cur_skid_pr").val(per3); 
    }
    if(s_money<=sum4) {s_money = Math.round(((money*100)/(100-per4))/rate);
    }
    if(s_money >= sum4)  {
        valuta = Math.round(((money*100)/(100-per4))/rate);
        proc = (valuta*rate)/100;
        jQuery("#cur_skid").val((per4*proc).toFixed(2));
        jQuery("#cur_skid_pr").val(per4); 
    }
    if(valuta < sum1)  {
       jQuery("#cur_skid").val('0.00');
        jQuery("#cur_skid_pr").val('0'); 
    }
       
    return valuta;  
}
function add_skidka(money,valuta){
    
    valuta=parseInt(valuta);
    money=parseFloat(money);
    sum1=parseInt(sum1);
    sum2=parseInt(sum2);
    sum3=parseInt(sum3);
    sum4=parseInt(sum4);
    proc = money/100;
    if((valuta >= sum1) && (valuta < sum2)){
        
        money = (money - (per1*proc)).toFixed(2);
        jQuery("#cur_skid").val((per1*proc).toFixed(2));
        jQuery("#cur_skid_pr").val(per1);
      
    }
    if((valuta >= sum2) && (valuta < sum3)){
        
        money = (money - (per2*proc)).toFixed(2);
        jQuery("#cur_skid").val((per2*proc).toFixed(2));
        jQuery("#cur_skid_pr").val(per2);
    }
    if((valuta >= sum3) && (valuta < sum4)){
      
        money = (money - (per3*proc)).toFixed(2);
        jQuery("#cur_skid").val((per3*proc).toFixed(2));
        jQuery("#cur_skid_pr").val(per3); 
    }
    
    if(valuta >= sum4)  {
       
        money = (money - (per4*proc)).toFixed(2);
        jQuery("#cur_skid").val((per4*proc).toFixed(2));
        jQuery("#cur_skid_pr").val(per4); 
    }
    if(valuta < sum1)  {
       jQuery("#cur_skid").val('0.00');
        jQuery("#cur_skid_pr").val('0'); 
    }
       
    return money;  
}

function key_valuta() {
valuta=jQuery("#valuta").val();
dengi=rate*valuta;
dengi=(dengi/curs).toFixed(2);
jQuery("#full_dengi").val((dengi));
dengi = add_skidka(dengi,valuta);
if (typeof kup_skidka!== 'undefined') {
   
    dengi=count_kupon (dengi); }

jQuery("#dengi").val((dengi));

};

function key_dengi() {
dengi=jQuery("#dengi").val();

dengi=(dengi*curs).toFixed(2);

if (typeof kup_skidka!== 'undefined') 
{
  dengi=count_kupon (dengi); 
  }
valuta=Math.round(dengi/rate);
valuta= add_skidka_val (dengi, valuta);
jQuery("#full_dengi").val(dengi);
jQuery("#valuta").val(valuta);
};

jQuery("#valuta").change( function() {
    valuta=jQuery("#valuta").val();
    valuta = parseInt(valuta);
     if(valuta <= min) {
        valuta=min;
        dengi=rate*valuta;
        dengi=(dengi/curs).toFixed(2);
jQuery("#full_dengi").val(dengi);                
        //dengi = add_skidka(dengi,valuta);
jQuery("#dengi").val((dengi));
jQuery("#valuta").val((min));   
    }
});
jQuery("#dengi").change( function() {
    valuta=jQuery("#valuta").val();
    valuta = parseInt(valuta);
     if(valuta <= min) {
        valuta=min;
        dengi=rate*valuta;
        dengi=(dengi/curs).toFixed(2)
jQuery("#full_dengi").val((dengi));                
        //dengi = add_skidka(dengi,valuta);
jQuery("#dengi").val((dengi));
jQuery("#valuta").val((min));   
    }
});

jQuery("#inputDelivery ").change( function() {
n_delivery=jQuery(this).find('option:selected').text();
      jQuery("#n_delivery").val(n_delivery);   
}); 
});

</script>

<div style="margin-left: 80px; margin-top: 20px;">
<div class="container">
				<form class="form-horizontal" action="" method="post"  name="zakaz" id="sale-gold">
					
					<input id="n_server" type="hidden" name='n_server' value="" />
					<input id="n_pay" type="hidden" name='n_pay' value="" />
                    <input id="n_delivery" type="hidden" name='n_delivery' value="" />
				    <input id="id_game" type="hidden" name='id_game' value="<?php if (isset($name_game['atts'])): echo $name_game['atts']; endif; ?>"/>
                    
                    <input id="name_valuta" type="hidden" name='name_valuta' value='0'/>
					<input id="full_dengi" type="hidden" name='full_dengi' value='0'/>
                    <input id="cur_skid" type="hidden" name='cur_skid' value='0.00'/>
                    <input id="cur_skid_pr" type="hidden" name='cur_skid_pr' value='0'/>                    
                    <input id="kup_skid" type="hidden" name='kup_skid' value='0.00'/>
                    <input id="kup_skid_pr" type="hidden" name='kup_skid_pr' value='0'/>                                        
                                        
                    <div class="form-group">
						<label class="col-xs-2 control-label" id="labelServer">Сервер *</label>
						<div class="col-xs-4">
							<select  id="inputServer" class="form-control" name="server_id">
                             <option value="">Выберите сервер</option>
                               <?php foreach ($this->data['servers'] as $key => $record): ?>
                                    <option value="<?php echo $record['ID'] ?>">
                                    <?php echo $record['name_server'] ?></option>
                                    <?php endforeach; ?>
                            </select><div class="add-down right">
								    <small id='rate_server'></small>
                                    
								</div>
						</div>
                    </div>
					
                    <div class="form-group">
						<label class="col-xs-2 control-label" id="labelPayment">Способ оплаты *</label>
						<div class="col-xs-4">
							<select id="inputPayment" class="form-control" name="payment_id">
                            <option value="">Выберите способ оплаты</option>
                                <?php foreach ($this->data['payments'] as $key => $record): ?>
                                    <option value="<?php echo $record['ID'] ?>">
                                    <?php echo $record['title_payment'] ?></option>
                                <?php endforeach; ?>
                            </select>
							
							<div class="add-down right">
							         <small id="messege"></small>
		                  </div>
                        </div>
					</div>
                    <div class="form-group">
						<label class="col-xs-2 control-label" id="labelDelivery">Способ доставки *</label>
						<div class="col-xs-4">
							<select id="inputDelivery" class="form-control" name="delivery">
                            <option value="">Выберите способ доставки</option>
                                <?php foreach ($this->data['delivery'] as $key => $record): ?>
                                    <option value="<?php echo $record['ID'] ?>">
                                    <?php echo $record['delivery'] ?></option>
                                <?php endforeach; ?>
                            </select>
						</div>
					</div>
                    
					<div class="form-group">
						<label  class="col-xs-2 control-label" id="labelPerson">Персонаж *</label>
						<div class="col-xs-4">
							<input id="inputPerson" class="form-control" name="person"  type="text" value="" />
						</div>
					</div>
					
					<div class="row">
						<label class="col-xs-2 control-label" id="labelCurrency">Валюта *</label>
						<div class="controls controls-row">
							
                            <div class="col-xs-2">
								<input  class="form-control" id="valuta" name="valuta" placeholder="Получу" type="text" " />
								<div class="add-down text-right">
								    <small id='paymethod_currency'>монет</small>
								</div>
							</div>
                            
                            <div class="col-xs-2">
							     <input id="dengi" name="dengi" class="form-control" placeholder="Заплачу" value="" type="text" />
							     <div class="add-down text-right">
							         <small id="n_valuta">руб.</small>
							     </div>
							</div>
					   </div>
					</div>
					
					
					<p class='game-title last'>Личная информация:</p>
					<div class="form-group">
						<label class="col-xs-2 control-label" id="labelEmail" >Email *</label>
						<div class="col-xs-4">
							<input id="inputEmail" class="form-control" name="email" placeholder="Email" type="text"  value="" />
						</div>
					</div>
                    
					<div id='kupon-group' class="form-group">
						<label class="col-xs-2 control-label" >Код скидки</label>
						<div class="col-xs-4">
                            <input id="kupon" class="form-control" name="kupon" placeholder="Если есть" type="text" onkeyup="dupon();"  />
						          <div class="add-down right">
							         <small id="res_kupon"></small>
							     </div>
                        
                        </div>
                        	
					</div>
                    
					<div class="form-group">
						<label class="col-xs-2 control-label" for="inputChar">Комментарии</label>
						<div class="col-xs-4">
							<textarea cols="40" class="form-control" id="inputComment" name="comment" placeholder="Способ связи с вами: ICQ, Skype." rows="2">
</textarea>
						</div>
					</div>
                    
					<div class="form-group">
						<label class="col-xs-2 control-label" for="inputChar"></label>
						<div class="col-xs-4">
                        <input type="hidden" name="action" value="buy_info" />
							<button type="submit" id='enter-form' class='btn btn-success btn-block'>Оформить покупку</button>
						</div>
					</div>
				</form>
</div></div>