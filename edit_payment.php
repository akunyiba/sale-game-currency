<!-- Блок настроек способов оплаты -->
<div style="margin-left:50px; margin-top:20px;">
<div class="container">
<h4>Настройки способов оплаты</h4>
<form class="form-horizontal" action="admin.php?page=edit-settings&action=submit" method="post"  name="zakaz" id="sale-gold">
   <?php if ($_POST['submit']=='add'){ ?>
   <p class='game-title first'>Создать новый способ оплаты</p>
     <div class="form-group">
        <label  class="col-xs-2 control-label" >Способ оплаты</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="title_payment" placeholder="способ оплаты" type="text" value="" />
        </div>
    </div>
    <?php } else { ?>
    <input type="hidden" name="id" value="<?php echo $this->data['payment']['ID'];?>" />
    <p class='game-title first'>Редактировать способ оплаты</p>
    <div class="form-group">
        <label class="col-xs-2 control-label" >Способ оплаты</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="title_payment" placeholder="способ оплаты" type="text" value="<?php echo $this->data['payment']['title_payment'];?>" />
        </div>
    </div>
    <?php } ?>
     <div class="form-group">
        <label  class="col-xs-2 control-label" >Логин</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="login" placeholder="" type="text" value="<?php echo $this->data['payment']['login'];?>" />
        </div>
    </div>
    <div class="form-group">
        <label  class="col-xs-2 control-label" >Пароль 1</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="pass1" placeholder="" type="text" value="<?php echo $this->data['payment']['pass1'];?>" />
        </div>
    </div>
    <div class="form-group">
        <label  class="col-xs-2 control-label" >Пароль 2</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="pass2" placeholder="" type="text" value="<?php echo $this->data['payment']['pass2'];?>" />
        </div>
    </div>
    <div class="form-group">
        <label  class="col-xs-2 control-label" >Название валюты
        </label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="name_valuta" placeholder="(руб.,$)" type="text" value="<?php echo $this->data['payment']['name_valuta'];?>" />
        <div class="add-down right">
							         <small >(руб/грн/$)</small>
							     </div>
        </div>
    </div>
    
   <div class="form-group">
        <label  class="col-xs-2 control-label" >Курс к рублю</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="rate_to_rub" placeholder="(руб.)" type="text" value="<?php echo $this->data['payment']['rate_to_rub'];?>" />
        </div>
    </div>
    
     <input type="hidden" name="field" value="payment" />
    <div class="form-group">
        <label class="col-xs-2 control-label" ></label>
        <div class="col-xs-3">
        <button type="submit" name="submit" value="save" class='btn btn-success btn-block'>Сохранить</button>
        </div>
    </div>
</form> 
</div>
</div>
<!-- Конец блока настроек способов оплаты -->