
<div style="margin-left:50px; margin-top:20px;">
<div class="container"><!-- Блок настроек игрового сервера -->
<h4>Настройки игрового сервера</h4>
<form class="form-horizontal" action="admin.php?page=edit-settings&action=submit" method="post"  name="zakaz" id="sale-gold">

<div class="form-group">
        <label class="col-xs-2 control-label" >Игра</label>
        <div class="col-xs-3">
        <select  id="inputServer" class="form-control" name="name_game">
            
        <?php foreach ($this->data['games'] as $key => $record): ?>
            <option value="<?php echo $record['ID']; ?>" <?php if ($record['ID'] == $this->data['server']['name_game'] ) echo 'selected="checked"'?>>
                <?php echo $record['name_game']; ?>
            </option>
        <?php endforeach; ?>
        </select>
        </div>
    </div>

<?php if ($_POST['submit']=='add') { ?>
    <p class='game-title first'>Добавить новый сервер игры</p>
     <div class="form-group">
        <label  class="col-xs-2 control-label">Название сервера</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="name_server" placeholder="сервер" type="text" value="" />
        </div>
    </div>
     <?php } else { ?>
     <input type="hidden" name="id" value="<?php echo $this->data['server']['ID'];?>" />
    <p class='game-title first'>Редактировать сервер игры</p>
    <div class="form-group">
        <label  class="col-xs-2 control-label">Название сервера</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="name_server" placeholder="сервер" type="text" value="<?php echo $this->data['server']['name_server'];?>" />
        </div>
    </div>
    <?php } ?>
    
      
    <div class="form-group">
        <label  class="col-xs-2 control-label">Минимальная сумма для продажи</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="min_sum" placeholder="минимальная сумма" type="text" value="<?php echo $this->data['server']['min_sum'];?>" />
        </div>
    </div>
    <div class="form-group">
        <label  class="col-xs-2 control-label" >Цена за 1 монету</label>
        <div class="col-xs-3">
        <input id="inputChar" class="form-control" name="server_price" placeholder="прайс" type="text" value="<?php echo $this->data['server']['server_price'];?>" />
        </div>
    </div>
    <input type="hidden" name="field" value="server" />
    <div class="form-group">
        <label class="col-xs-2 control-label" ></label>
        <div class="col-xs-3">
        <button type="submit" name="submit" value="save"  class='btn btn-success btn-block'>Сохранить</button>
        </div>
    </div>
</form>
</div>
</div>
