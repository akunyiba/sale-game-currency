jQuery(document).ready( function() {
 
jQuery('#sale-gold').bind('submit', function(event) {
  jQuery('[name=email]').each(function() {
    if(!jQuery(this).val().length) {
      event.preventDefault();
      jQuery('#labelEmail').css('color', 'red');
    }
  });
  jQuery('[name=person]').each(function() {
    if(!jQuery(this).val().length) {
      event.preventDefault();
      jQuery('#labelPerson').css('color', 'red');
    }
  });
  jQuery('[name=delivery]').each(function() {
    if(!jQuery(this).val().length) {
      event.preventDefault();
      jQuery('#labelDelivery').css('color', 'red');
    }
  });
   jQuery('[name=server_id]').each(function() {
    if(!jQuery(this).val().length) {
      event.preventDefault();
      jQuery('#labelServer').css('color', 'red');
    }
  });
  jQuery('[name=payment_id]').each(function() {
    if(!jQuery(this).val().length) {
      event.preventDefault();
      jQuery('#labelPayment').css('color', 'red');
    }
  });
  jQuery('[name=valuta]').each(function() {
    if(!jQuery(this).val().length) {
      event.preventDefault();
      jQuery('#labelCurrency').css('color', 'red');
    }
  });
});

})

