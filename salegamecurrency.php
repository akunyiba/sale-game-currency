<?php
/*
 Plugin Name: Sale of Game Currency
 Description: Продажа игровых валют
 Version: 0.1
 Author:  mmogoldbox
 Author URI: http://mmogoldbox.com
*/
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

if (!class_exists('SaleGameCurrency')) {
 class SaleGameCurrency {
 

	public $data = array();
	
    
    
	
	function SaleGameCurrency()
	{
		global $wpdb;
		DEFINE('SaleGameCurrency', true);
		$this->plugin_name = plugin_basename(__FILE__);
		$this->plugin_url = trailingslashit(WP_PLUGIN_URL.'/'.dirname(plugin_basename(__FILE__)));
		
		$this->tbl_sgc_servers   = $wpdb->prefix . 'sgc_servers';
        $this->tbl_sgc_payments   = $wpdb->prefix . 'sgc_payments';
        $this->tbl_sgc_buyers   = $wpdb->prefix . 'sgc_buyers';
        $this->tbl_sgc_delivery   = $wpdb->prefix . 'sgc_delivery';
        $this->tbl_sgc_games   = $wpdb->prefix . 'sgc_games';
        $this->tbl_sgc_skidki   = $wpdb->prefix . 'sgc_skidki';
		
		
		register_activation_hook( $this->plugin_name, array(&$this, 'activate') );
		register_deactivation_hook( $this->plugin_name, array(&$this, 'deactivate') );
		register_uninstall_hook( $this->plugin_name, array(&$this, 'uninstall') );
		
		
		if ( is_admin() ) {

            wp_enqueue_script( 'my-ajax-request', plugin_dir_url( __FILE__ ) . 'js/my_query.js', array( 'jquery' ) );
            wp_localize_script( 'my-ajax-request', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
            add_action( 'wp_ajax_myajax-submit', array(&$this,'myajax_submit' ));
            add_action( 'wp_print_styles', array(&$this,'admin_load_scripts') );
            add_action('wp_print_scripts', array(&$this, 'admin_load_scripts'));
            add_action( 'admin_menu', array(&$this, 'admin_generate_menu') );
			
		} else {
            wp_enqueue_script( 'my-ajax-request', plugin_dir_url( __FILE__ ) . 'js/my_query.js', array( 'jquery' ) );
            wp_localize_script( 'my-ajax-request', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
            add_action( 'wp_ajax_nopriv_myajax-submit', array(&$this,'myajax_submit' ));
            add_action('wp_print_scripts', array(&$this, 'site_load_scripts'));
            //add_action('wp_print_styles', array(&$this, 'site_load_styles'));
            add_action('init', array(&$this,'sgc_run'));
            add_shortcode('show_sgc', array (&$this, 'site_show_sgc'));
            add_shortcode('show_info', array (&$this, 'payment_info'));
		}
	}
	
public function sgc_run(){
    global $wpdb;
    
    $this->data['link'] = $wpdb->get_var("SELECT link FROM `" . $this->tbl_sgc_skidki . "`");
    $result_url = $this->data['link'];
    preg_match ('/^http(s)?\:\/\/[^\/]+\/(.*)$/i',$result_url, $matches);
    $real_url = $_SERVER['REQUEST_URI'];
    preg_match ('/^\/([^\?]*)(\?.+)?$/i',$real_url, $real_matches);
    $matches[2]='result/';
    if($real_matches[1] == $matches[2])
    {
        if(isset($_POST['SignatureValue']))
        {
        $this->robokassa_process();
        }
        if(isset($_POST['ik_payment_id']))
        {
        $this->interkassa_process();
        }
        if(isset($_REQUEST["notification_type"]))
        {
        $this->yandex_process();
        }
        if(isset($_REQUEST["LMI_PAYEE_PURSE"]))
        {
        $this->webmoney_process();
        }
    }
}
    
public function robokassa_process (){
        global $wpdb;
        
        $this->data['pay'] = $wpdb->get_var("SELECT pass2 FROM `" . $this->tbl_sgc_payments . "` WHERE `title_payment`= 'Robokassa'");
        $mrh_pass2 = $this->data['pay'];
        $tm=getdate(time()+9*3600);
        $date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";
        $out_summ = $_REQUEST["OutSum"];
        $inv_id = $_REQUEST["InvId"];
        $shp_item = $_REQUEST["Shp_item"];
        $crc = $_REQUEST["SignatureValue"];

        $crc = strtoupper($crc);
        $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:Shp_item=$shp_item"));
        if ($my_crc !=$crc)
        {	
          echo "bad sign\n";
          exit();
        }
        echo "OK$inv_id\n";
        $status = array('status' => 1);
        $wpdb->update( $this->tbl_sgc_buyers, $status, array( 'ID' => $inv_id ));  
        $this->sendEmail($inv_id,$out_summ);
        
} 

public function yandex_process (){
        global $wpdb;
        
        $this->data['pay'] = $wpdb->get_var("SELECT pass1 FROM `" . $this->tbl_sgc_payments . "` WHERE `title_payment`= 'Яндекс Деньги'");
        $notification_secret = $this->data['pay'];
        
        $notification_type = $_REQUEST["notification_type"];
        $operation_id = $_REQUEST["operation_id"];
        $amount = $_REQUEST["amount"];
        $withdraw_amount = $_REQUEST["withdraw_amount"];
        $currency = $_REQUEST["currency"];
        $datetime = $_REQUEST["datetime"];
        $sender = $_REQUEST["sender"];
        $codepro = $_REQUEST["codepro"];
        $label = $_REQUEST["label"];
        
        $sha1_hash = $_REQUEST["sha1_hash"];
        
        $stroka= $notification_type."&".$operation_id."&".$amount."&".$currency."&".
        $datetime."&".$sender."&".$codepro."&".$notification_secret."&".$label;
        
         
        $sha1 = hash("sha1", $stroka); //кодируем в SHA1

        //Ниже - проверка на валидность
        if ( $sha1 == $sha1_hash )
        {
        echo 'OK';
        $status = array('status' => 1);
        $wpdb->update( $this->tbl_sgc_buyers, $status, array( 'ID' => $label ));  
        
        $this->sendEmail($label,$amount);
        
        }
        else
        {
        echo "bad sign\n";
        exit();
        } 
} 


public function interkassa_process ()
{
        global $wpdb;
        
        
}

public function webmoney_process (){
        global $wpdb;
        $LMI_PAYEE_PURSE = $_REQUEST["LMI_PAYEE_PURSE"];
        $this->data['pay'] = $wpdb->get_var("SELECT pass1 FROM `" . $this->tbl_sgc_payments . "` WHERE `login`='".$LMI_PAYEE_PURSE."'");
        $secret_key = $this->data['pay'];
               
        $LMI_PAYMENT_AMOUNT = $_REQUEST["LMI_PAYMENT_AMOUNT"];
        $LMI_PAYMENT_NO = $_REQUEST["LMI_PAYMENT_NO"];
        $LMI_MODE = $_REQUEST["LMI_MODE"];
        $LMI_SYS_INVS_NO = $_REQUEST["LMI_SYS_INVS_NO"];
        $LMI_SYS_TRANS_NO = $_REQUEST["LMI_SYS_TRANS_NO"];
        $LMI_SYS_TRANS_DATE = $_REQUEST["LMI_SYS_TRANS_DATE"];
        $LMI_SECRET_KEY = $_REQUEST["LMI_SECRET_KEY"];
        $LMI_PAYER_PURSE = $_REQUEST["LMI_PAYER_PURSE"];
        $LMI_PAYER_WM = $_REQUEST["LMI_PAYER_WM"];
        
        $LMI_HASH = $_REQUEST["LMI_HASH"];
    
        $common_string = $LMI_PAYEE_PURSE.$LMI_PAYMENT_AMOUNT.$LMI_PAYMENT_NO.
        $LMI_MODE.$LMI_SYS_INVS_NO.$LMI_SYS_TRANS_NO.
        $LMI_SYS_TRANS_DATE.$secret_key.$LMI_PAYER_PURSE.$LMI_PAYER_WM;

        $hash = strtoupper(md5($common_string));
        if ($hash != $LMI_HASH)
        {	
          exit();
        }
        echo "Yes";
        
        $status = array('status' => 1);
        $wpdb->update( $this->tbl_sgc_buyers, $status, array( 'ID' => $LMI_PAYMENT_NO ));  
        $this->sendEmail($LMI_PAYMENT_NO,$LMI_PAYMENT_AMOUNT);
        
} 

private function sendEmail($id,$amounts)
{
    global $wpdb;
    $to=get_option('admin_email');
        $from= get_option('admin_email');
        $message="Заказ № ".$id."оплачен! Сумма покупки: ".$amounts;
        $subject="Информация об оплате";
        $headers = 'From: MMOGOLDBOX <'.$from.'>' . "\r\n";
        wp_mail( $to, $subject, $message, $headers, $attachments );
        
        $this->data['email'] = $wpdb->get_var("SELECT email FROM `" . $this->tbl_sgc_buyers . "` WHERE `ID`= ".$id);
        
        $to=$this->data['email'];
        $from = get_option('admin_email');
        $message="Номер заказа № ".$id." Вы заказали игровые деньги на сумму: ".$amounts;
        $subject="Информация о заказе.";
        $headers = 'From: MMOGOLDBOX <'.$from.'>' . "\r\n";
        wp_mail( $to, $subject, $message, $headers, $attachments );
}

  
function myajax_submit() {
    global $wpdb;

    if($_POST['postID']){
    $postID = $_POST['postID'];   
    $this->data['servers'] = $wpdb->get_results("SELECT ID, name_server FROM `" . $this->tbl_sgc_servers . "` WHERE `name_game`= ".$postID, ARRAY_A);
    $r = $this->data['servers'];
    $response = json_encode($r);
    header( "Content-Type: application/json" );
    echo $response;
    }

    if($_POST['email']){
    $email = $_POST['email']; 
    $kupon = $_POST['kupon'];
    $this->data['buyers'] = $wpdb->get_results("SELECT email, kupon, skidka FROM `" . $this->tbl_sgc_buyers . "` WHERE `kupon`= '". $kupon ."'", ARRAY_A);
    if ($this->data['buyers'][0]['skidka'])
    {   
        if ($this->data['buyers'][0]['email'] == $email)
        {
          $r = $this->data['buyers'][0]['skidka'];
          $response = json_encode($r);  
        }
        else 
        {
            $r=0;
            $response = json_encode($r); 
        }
    }    
    else 
    {
       $r=0;
       $response = json_encode($r); 
    }
    header( "Content-Type: application/json" );
    echo $response;    
    }
    exit;
    }

    
    
	/**
	 * Загрузка необходимых скриптов для страницы управления 
	 * в панели администрирования
	 */
	function admin_load_scripts()
	{   wp_register_style('SGCCss', $this->plugin_url .'css/bootstrap.css', true);
        wp_enqueue_style('SGCCss');
		// Региестрируем скрипты
		wp_register_script('SGCJs', $this->plugin_url . 'js/bootstrap.min.js' );
		//wp_register_script('jquery', $this->plugin_url . 'js/jquery-1.4.2.min.js' );
        wp_register_script('SGCAdminJs', $this->plugin_url . 'js/admin-scripts.js' );
		
		// Добавляем скрипты на страницу
		wp_enqueue_script('SGCJs');
		//wp_enqueue_script('jquery');
        wp_enqueue_script('SGCAdminJs');
        
	}
	/**
 	 * Генерируем меню
	 */
	function admin_generate_menu()
    
	{
		// Добавляем основной раздел меню
		add_menu_page('Настройки плагина продаж игровых валют', 'SGC', 'manage_options', 'edit-settings', array(&$this, 'admin_edit_settings'));
		// Добавляем дополнительный раздел
		add_submenu_page( 'edit-settings', 'Управление содержимом', 'Заказы', 'manage_options', 'buyers_info', array(&$this,'admin_buyers_info'));
        add_submenu_page( 'edit-settings', 'Управление содержимом', 'Покупки', 'manage_options', 'stuff_info', array(&$this,'admin_stuff_info'));
	}
	
    
	/**
	 * Выводим список отзывов для редактирования
	 */
	public function admin_edit_settings()
	{
		global $wpdb; 
      
		
        $action = isset($_GET['action']) ? $_GET['action'] : null ;
        
        
                           
		$submit = $_POST['submit'];
        
         switch ($submit)
                 {
                        case "edit": 
                            if ($_POST['name_field']=='no'){
                            echo '<div class="updated"><p><strong>Нет сервевра! Для редакции, добавьте сервер!</strong></p></div>';
                             $this->admin_show_settings();
                            }    
                            else {  
                                 if($_POST['field']=='game') {
                                    $this->data['game'] = $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_games . "` WHERE `ID`= ". (int)$_POST['name_field'], ARRAY_A);
				                    include_once('edit_game.php');
                                    }
                                 if($_POST['field']=='server') {
                                    $this->data['games'] = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_games . "`", ARRAY_A);
		                            $this->data['server'] = $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_servers . "` WHERE `ID`= ". (int)$_POST['name_field'], ARRAY_A);
				                    include_once('edit_server.php');
                                    }
                                 
                                 if($_POST['field']=='delivery') {
                                    $this->data['delivery'] = $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_delivery . "` WHERE `ID`= ". (int)$_POST['name_field'], ARRAY_A);
				                    include_once('edit_delivery.php');
                                    }
                                 
                                 
                                 if($_POST['field']=='payment') {
                                    $this->data['payment'] = $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_payments . "` WHERE `ID`= ". (int)$_POST['name_field'], ARRAY_A);
				                    include_once('edit_payment.php');
                                    }
                                }
                               
                         break;

                         case "add": 
                         
                                if($_POST['field']=='game') {
                                 $this->data['game'] 	= $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_games . "` WHERE `ID`= ". (int)$_POST['name_field'], ARRAY_A);
				                 include_once('edit_game.php');
                                    
                                    
                                } 
                                if($_POST['field']=='server') {
                                    $this->data['games'] = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_games . "`", ARRAY_A);
		                            include_once('edit_server.php');
                                    }
                                    if($_POST['field']=='payment') {
                                    
		                            include_once('edit_payment.php');
                                    }
 
                                if($_POST['field']=='delivery') include_once('edit_delivery.php');
                            break;

                        case "del":
                                if($_POST['field']=='game')
                                $wpdb->query("DELETE FROM `".$this->tbl_sgc_games."` WHERE `ID` = '". (int)$_POST['name_field'] ."'");
                                if($_POST['field']=='server')
                                $wpdb->query("DELETE FROM `".$this->tbl_sgc_servers."` WHERE `ID` = '". (int)$_POST['name_field'] ."'");
                                if($_POST['field']=='payment')
                                $wpdb->query("DELETE FROM `".$this->tbl_sgc_payments."` WHERE `ID` = '". (int)$_POST['name_field'] ."'");
                                if($_POST['field']=='delivery')
                                $wpdb->query("DELETE FROM `".$this->tbl_sgc_delivery."` WHERE `ID` = '". (int)$_POST['name_field'] ."'");
                        
                        $this->admin_show_settings();
                         break;
              
		
		
			case 'save':
                        if($_POST['field']=='skidki'){
                               $inputData = array(
					               'sum1' 	  	  => strip_tags($_POST['sum1']),
                                   'percent1' 	  => strip_tags($_POST['percent1']),
					               'sum2' 		  => strip_tags($_POST['sum2']),
					               'percent2' 	  => strip_tags($_POST['percent2']),
                                   'sum3' 	  => strip_tags($_POST['sum3']),
					               'percent3' 	  => strip_tags($_POST['percent3']),
                                   'sum4' 	  => strip_tags($_POST['sum4']),
                                   'percent4' 	  => strip_tags($_POST['percent4']),
                                   'status' 	  => strip_tags($_POST['status']),
                                   'link'        =>       strip_tags($_POST['link'])
					               );

				           $count_field = $wpdb->get_var("SELECT COUNT(*) FROM `" . $this->tbl_sgc_skidki . "`");
                            if ($count_field) 
                                $wpdb->update( $this->tbl_sgc_skidki, $inputData, array( 'ID' => 1 ));
                                
                            else
				            $wpdb->insert( $this->tbl_sgc_skidki, $inputData );
                                }
                        if($_POST['field']=='payment'){
                            
				                $inputData = array(
					               'title_payment' 	  	  => strip_tags($_POST['title_payment']),
                                   'rate_to_rub' 	  => strip_tags($_POST['rate_to_rub']),
					               'name_valuta' 		  => strip_tags($_POST['name_valuta']),
                                   'login' 	  => trim(strip_tags($_POST['login'])),
					               'pass1' 	  => trim(strip_tags($_POST['pass1'])),
                                   'pass2' 	  => trim(strip_tags($_POST['pass2'])),
                                   );
			
				            $editId=intval($_POST['id']);
			
				            if ($editId) 
                                $wpdb->update( $this->tbl_sgc_payments, $inputData, array( 'ID' => $editId ));
                            else $wpdb->insert( $this->tbl_sgc_payments, $inputData );
                                }
                                     
                        if($_POST['field']=='server'){
                            
                            
				                $inputData = array(
					               'name_game' 	  	  => strip_tags($_POST['name_game']),
                                   'name_server' 	  => strip_tags($_POST['name_server']),
					               'min_sum' 		  => strip_tags($_POST['min_sum']),
					               'server_price' 	  => strip_tags($_POST['server_price']),
					               );
			
				            $editId=intval($_POST['id']);
			
				            if ($editId) 
                                $wpdb->update( $this->tbl_sgc_servers, $inputData, array( 'ID' => $editId ));
                            else $wpdb->insert( $this->tbl_sgc_servers, $inputData );
                                }
                            
                        if($_POST['field']=='game'){
				                $inputData = array(
					               'name_game' 	  	  => strip_tags($_POST['name_game'])
					               );
			
				            $editId=intval($_POST['id']);
		
				            if ($editId)
                            $wpdb->update( $this->tbl_sgc_games, $inputData, array( 'ID' => $editId ));
                            else
                            $wpdb->insert( $this->tbl_sgc_games, $inputData );
                          }
                           if($_POST['field']=='delivery'){
				                $inputData = array(
					               'delivery' 	  	  => strip_tags($_POST['delivery'])
					               );
			
				            $editId=intval($_POST['id']);
		
				            if ($editId)
                            $wpdb->update( $this->tbl_sgc_delivery, $inputData, array( 'ID' => $editId ));
                            else
                            $wpdb->insert( $this->tbl_sgc_delivery, $inputData );
                          }
				echo '<div class="updated"><p><strong>Настройки успешно сохранены</strong></p></div>';    
                $this->admin_show_settings();
			break;
		default:
				$this->admin_show_settings();
		}
       	
	}
	
   
	private function admin_show_settings()
	{
		global $wpdb;
	// Получаем данные из БД
		$this->data['games'] 	 = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_games . "`", ARRAY_A);
       	$this->data['servers'] 	 = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_servers . "`", ARRAY_A);
		$this->data['payments'] 	 = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_payments . "`", ARRAY_A);
        $this->data['deliveries'] 	 = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_delivery . "`", ARRAY_A);
        $this->data['skidki'] = $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_skidki . "` WHERE `ID`= 1"  , ARRAY_A);
       
		include_once('set_general.php');
    }
	
    public function admin_buyers_info()
	{  
	   global $wpdb;
	    $action = isset($_GET['action']) ? $_GET['action'] : null ;
        if($action){
        if($action =='buy_del'){
            $wpdb->query("DELETE FROM `".$this->tbl_sgc_buyers."` WHERE `ID` = '".  (int)$_GET['id']  ."'");
            echo '<div class="updated"><p><strong>Покупатель удален.</strong></p></div>';    
            $this->buyers_show();
		
         }
        if($action =='del_all'){ 
            
            if (isset ($_POST['checkbox']))
            { 
                $id =  $_POST['checkbox']; 
                $allbox = "(";
                foreach($id as $val) $allbox.= "$val,";
                $allbox = substr($allbox, 0, strlen($allbox) - 1 ). ")" ;
            
            $wpdb->query("DELETE FROM `".$this->tbl_sgc_buyers."` WHERE `ID` IN ".$allbox);
            echo '<div class="updated"><p><strong>Покупатели удалены.</strong></p></div>';    
            $this->buyers_show();
		}
        else {
            echo '<div class="updated"><p><strong>Никто не отмечен, для удаления отметьте покупателей</strong></p></div>';    
            $this->buyers_show();
        }
         
         }  
       }
       else $this->buyers_show();
      
	}
    private function buyers_show()
	{  global $wpdb;
	    $this->data['buyers'] 	 = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_buyers . "` ORDER BY `date_sell` DESC", ARRAY_A);
       include_once('buyers_info.php');
    }
    
    public function admin_stuff_info()
	{  
	   global $wpdb;
       	
        $action = isset($_GET['action']) ? $_GET['action'] : null ;
        if($action)
        {
            if($action =='kupon')
            {   
                $id=$_GET['id'];
                $email=$_GET['email'];
                $kupon=md5(date("His"));
                include_once('kupon.php');   
            }
             if($_POST['submit'])
            {
              $data = array(
              'kupon' => $_POST['kupon'],
              'skidka' => $_POST['skidka']
              );
                $wpdb->update( $this->tbl_sgc_buyers, $data, array( 'ID' => $_POST['id'] ));  
        
                $to=$_POST['email'];
                $message="Информация о заказе.";
                $from= get_option('admin_email');
                $message=$_POST['message'];
                $subject="Купон на скидку";
                $headers = 'From: MMOgoldbox <'.$from.'>' . "\r\n";
                wp_mail( $to, $subject, $message, $headers );
        
                echo '<div class="updated"><p><strong>Купон отправлен покупателю '.$to.'</strong></p></div>';    
                $this->stuff_show(); 
            }
        }
      else $this->stuff_show();
    }
	
    	
    private function stuff_show()
	{     
        global $wpdb;
	    $this->data['buyers'] = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_buyers . "` WHERE `status` = 1", ARRAY_A);
        $this->data['skidka'] = $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_skidki ."` WHERE `ID`= 1" , ARRAY_A);
      
      if($this->data['buyers']){
        foreach($this->data['buyers'] as $value) {
           $emails[] =$value['email'];
        }
        $unic_email = array_unique ($emails);
        
        foreach ($unic_email as $email)
       {    
            ++$i;$all_sum =0;$count_buy=0; 
            foreach ($this->data['buyers'] as $value)
            {   
                if ($value['email'] == $email)
                {
                    ++$count_buy;
                     $all_sum = $all_sum + $value['valuta'];
                     $buyer[$i]['id'] = $value['ID'];
                     if ($value['kupon'])
                     { 
                     $buyer[$i]['kupon'] = $value['kupon'];
                     $buyer[$i]['kup_skid'] = $value['skidka'];
                     
                     }
                }
            }
           if ($all_sum < $this->data['skidka']['sum1'])
            $buyer[$i]['cur_skid'] = 0;
            if (($all_sum > $this->data['skidka']['sum1']) && ($all_sum <= $this->data['skidka']['sum2']))
            $buyer[$i]['cur_skid'] = $this->data['skidka']['percent1'];
            if (($all_sum > $this->data['skidka']['sum2']) && ($all_sum <= $this->data['skidka']['sum3']))
            $buyer[$i]['cur_skid'] = $this->data['skidka']['percent2'];
            if (($all_sum > $this->data['skidka']['sum3']) && ($all_sum <= $this->data['skidka']['sum4']))
            $buyer[$i]['cur_skid'] = $this->data['skidka']['percent3'];
            if ($all_sum > $this->data['skidka']['sum4'])
            $buyer[$i]['cur_skid'] = $this->data['skidka']['percent4'];
            
            $buyer[$i]['all_sum'] = $all_sum;
            $buyer[$i]['email'] = $email;
            $buyer[$i]['count_buy'] = $count_buy;
        }
       
      include_once('stuff_info.php');
      
      }
      else echo("<h3>В базе отсутствуют оплаченные покупки.</h3>");
    }   
        
        
        function site_load_scripts()
	{
        wp_register_style('SGCCss', $this->plugin_url .'css/bootstrap.css', true);
        wp_enqueue_style('SGCCss');
		wp_register_script('SGCJs', $this->plugin_url . 'js/bootstrap.min.js' );
		//wp_register_script('jquery', $this->plugin_url . 'js/jquery-1.4.2.min.js' );
        wp_enqueue_script('SGCJs');
		//wp_enqueue_script('jquery');
        
	}



	public function site_show_sgc($name_game, $content=null)
	{
		global $wpdb;
        
	
		if (isset($_POST['action']) && $_POST['action'] == 'buy_info') {
		  
			$this->payment_info($name_game);
		}
	else{
            $this->data['servers'] = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_servers . "` WHERE `name_game`='".$name_game['atts']."'", ARRAY_A);
            $this->data['payments'] = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_payments . "`", ARRAY_A);
            $this->data['delivery'] = $wpdb->get_results("SELECT * FROM `" . $this->tbl_sgc_delivery . "`", ARRAY_A);
            $this->data['skidki'] = $wpdb->get_results("SELECT sum1,percent1,sum2,percent2,sum3,percent3,sum4,percent4,status FROM `" . $this->tbl_sgc_skidki . "`", ARRAY_A);
            if ($this->data['skidki'][0]['status']==1)
            $skidki = json_encode($this->data['skidki']);
            //var_dump($this->data['skidki']);
            $servers = json_encode($this->data['servers']);
            $payments = json_encode($this->data['payments']);
            
            //
            ## Включаем буферизацию вывода
            ob_start ();
            include_once('sell_form.php');
            ## Получаем данные
            $output = ob_get_contents ();
            ## Отключаем буферизацию
            ob_end_clean ();
            
            return $output;}
	}
	
	public function payment_info() 
	{
		global $wpdb;
        if ($_POST['id_game'])
        {
        $this->data['game'] = $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_games ."` WHERE `ID`='".$_POST['id_game'] ."'", ARRAY_A);
		$inputData = array(
			'email' 	  	     => strip_tags($_POST['email']),
			'n_game' 		     => strip_tags($this->data['game']['name_game']),
			'n_server' 	         => strip_tags($_POST['n_server']),
			'n_pay'              => strip_tags($_POST['n_pay']),
            'n_delivery' 	  	 => strip_tags($_POST['n_delivery']),
			'person' 		     => strip_tags($_POST['person']),
			'valuta' 	         => strip_tags($_POST['valuta']),
			'dengi'              => strip_tags($_POST['dengi']),
            'comment'            => strip_tags($_POST['comment']),
            'status'            => '0',
            
		);
		$wpdb->insert( $this->tbl_sgc_buyers, $inputData );
        $n_zakaz = $wpdb->insert_id;
            $this->data['pay'] = $wpdb->get_row("SELECT * FROM `" . $this->tbl_sgc_payments . "` WHERE `title_payment`='".$_POST['n_pay'] ."'", ARRAY_A);
            
            if ($_POST['n_pay'] == "Robokassa"){
                include_once('robokassa.php');
            }
            if ($_POST['n_pay'] == "Интеркасса"){
                include_once('interkassa.php');
            }
            if ($_POST['n_pay'] == "Яндекс Деньги"){
                include_once('yandex_dengi.php');
            }
            if ($_POST['n_pay'] == "WMZ"){
                include_once('webmoney.php');
            }
            if ($_POST['n_pay'] == "WMR"){
                include_once('webmoney.php');
            }
            if ($_POST['n_pay'] == "WMU"){
                include_once('webmoney.php');
            }
        include_once('info.php');
        }
       
       
       if ($_REQUEST["SignatureValue"]){
        $this->data['pay'] = $wpdb->get_var("SELECT pass1 FROM `" . $this->tbl_sgc_payments . "` WHERE `title_payment`= 'Robokassa'");
        $mrh_pass1 = $this->data['pay'];
        
        include_once('robo_success.php');
       }
       if (!$_REQUEST["SignatureValue"] && isset($_REQUEST["InvId"])){
        
        include_once('robo_fail.php');
       }
       if ($_REQUEST["LMI_PAYMENT_NO"]){
        
        include_once('robo_success.php');
       }
	}
	
	
	/**
	 * Активация плагина
	 */
	function activate() 
	{
		global $wpdb;
		
		require_once(ABSPATH . 'wp-admin/upgrade-functions.php');
		$table1	= $this->tbl_sgc_games;
		$table2	= $this->tbl_sgc_servers;
        $table3	= $this->tbl_sgc_payments;
        $table4	= $this->tbl_sgc_buyers;
        $table5	= $this->tbl_sgc_skidki;
        $table6	= $this->tbl_sgc_delivery;
        
		
		## Определение версии mysql
		if ( version_compare(mysqli_get_server_info(), '4.1.0', '>=') ) {
			if ( ! empty($wpdb->charset) )
				$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
			if ( ! empty($wpdb->collate) )
				$charset_collate .= " COLLATE $wpdb->collate";
		}
		
		
        $sql_table_sgc_games = "
				CREATE TABLE `".$wpdb->prefix."sgc_games` (
					`ID` INT(10) UNSIGNED NULL AUTO_INCREMENT,
					`name_game` VARCHAR(200) NULL,
					PRIMARY KEY (`ID`)
				)".$charset_collate.";";
		
		## Проверка на существование таблицы	
		if ( $wpdb->get_var("show tables like '".$table1."'") != $table1 ) {
			dbDelta($sql_table_sgc_games);
		}
        ## Структура нашей таблицы для отзывов
		$sql_table_sgc_servers = "
				CREATE TABLE `".$wpdb->prefix."sgc_servers` (
					`ID` INT(10) UNSIGNED NULL AUTO_INCREMENT,
					`name_game` VARCHAR(200) NULL,
					`name_server` VARCHAR(200) NULL,
					`min_sum` INT(6) UNSIGNED NULL,
					`server_price` FLOAT NULL,
					PRIMARY KEY (`ID`)
				)".$charset_collate.";";
		
		## Проверка на существование таблицы	
		if ( $wpdb->get_var("show tables like '".$table2."'") != $table2 ) {
			dbDelta($sql_table_sgc_servers);
		}
        $sql_table_sgc_payments = "
				CREATE TABLE `".$wpdb->prefix."sgc_payments` (
					`ID` INT(10) UNSIGNED NULL AUTO_INCREMENT,
					`title_payment` VARCHAR(255) NOT NULL DEFAULT '0',
					`rate_to_rub` FLOAT NULL,
					`name_valuta` VARCHAR(200) NULL,
                    `login` VARCHAR(200) NULL,
                    `pass1` VARCHAR(200) NULL,
					`pass2` VARCHAR(200) NULL,
                   	PRIMARY KEY (`ID`)
				)".$charset_collate.";";
		
		## Проверка на существование таблицы	
		if ( $wpdb->get_var("show tables like '".$table3."'") != $table3 ) {
			dbDelta($sql_table_sgc_payments);
		}
        $sql_table_sgc_buyers = "
				CREATE TABLE `".$wpdb->prefix."sgc_buyers` (
					`ID` INT(10) UNSIGNED NULL AUTO_INCREMENT,
					`email` VARCHAR(200) NOT NULL DEFAULT '0',
                    `n_game` VARCHAR(200) NULL,
					`n_server` VARCHAR(200) NULL,
                    `n_pay` VARCHAR(200) NULL,
                    `n_delivery` VARCHAR(200) NULL,
					`person` VARCHAR(200) NULL,
					`valuta`  FLOAT NULL,
					`dengi`  FLOAT NULL,
                    `kupon`  INT(14) UNSIGNED NULL ,
                    `skidka`  FLOAT NULL,
                    `comment` TEXT NOT NULL,
                    `status` INT(2) UNSIGNED NULL, 
                    `date_sell` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
					PRIMARY KEY (`ID`)
				)".$charset_collate.";";
		
		## Проверка на существование таблицы	
		if ( $wpdb->get_var("show tables like '".$table4."'") != $table4 ) {
			dbDelta($sql_table_sgc_buyers);
		}
        $sql_table_sgc_skidki = "
				CREATE TABLE `".$wpdb->prefix."sgc_skidki` (
					`ID` INT(10) UNSIGNED NULL AUTO_INCREMENT,
					`sum1` INT(10) UNSIGNED NULL,
					`percent1` FLOAT NULL,
                    `sum2` INT(10) UNSIGNED NULL,
                    `percent2` FLOAT NULL,
                    `sum3` INT(10) UNSIGNED NULL,
                    `percent3` FLOAT NULL,
                    `sum4` INT(10) UNSIGNED NULL,
                    `percent4` FLOAT NULL,
                    `status` INT(2) UNSIGNED NULL,
                    `link` TEXT NULL,                 
					PRIMARY KEY (`ID`)
				)".$charset_collate.";";
		
		## Проверка на существование таблицы	
		if ( $wpdb->get_var("show tables like '".$table5."'") != $table5 ) {
			dbDelta($sql_table_sgc_skidki);
		}
		$sql_table_sgc_delivery = "
				CREATE TABLE `".$wpdb->prefix."sgc_delivery` (
					`ID` INT(10) UNSIGNED NULL AUTO_INCREMENT,
					`delivery` VARCHAR(200) NULL,
					PRIMARY KEY (`ID`)
				)".$charset_collate.";";
		
		## Проверка на существование таблицы	
		if ( $wpdb->get_var("show tables like '".$table6."'") != $table6 ) {
			dbDelta($sql_table_sgc_delivery);
		}
	}
	
	function deactivate() 
	{
		return true;
	}
	
	/**
	 * Удаление плагина
	 */
	function uninstall() 
	{
		global $wpdb;
		$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}sgc_games");
		$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}sgc_servers");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}sgc_payments");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}sgc_buyers");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}sgc_skidki");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}sgc_delivery");
	}

 }
 }
global $settings;
$settings = new SaleGameCurrency();

?>